import tornado.ioloop
import json
import tornado.web
from tornado import gen, web
from sqlalchemy import create_engine, inspect
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from concurrent.futures import ThreadPoolExecutor
from tornado.concurrent import return_future, run_on_executor
from tornado.ioloop import IOLoop
import time

tp_executor = ThreadPoolExecutor(10)

engine = create_engine('sqlite:///db.sqlite', echo=True)
Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)

    def __init__(self, id=None, name=None):
        self.id = id
        self.name = name


class MainController(object):
    def __init__(self, db_session, model_class, io_loop=None,
                 executor=tp_executor):
        self.io_loop = io_loop or IOLoop.instance()
        self.executor = executor
        self.db_session = db_session
        self.model_class = model_class

    @run_on_executor
    @return_future
    def get(self, id: int, callback=None):
        session = self.db_session()
        try:
            result = session.query(self.model_class).get(id)
        except Exception as e:
            result = e
        session.close()
        callback(result)

    @run_on_executor
    @return_future
    def get_all(self, callback=None):
        session = self.db_session()
        try:
            result = session.query(self.model_class).all()
        except Exception as e:
            result = e
        session.close()
        callback(result)


    def create(self, item, callback=None):
        session = self.db_session()
        try:
            session.add(item)
            # TODO: Uncomment for demo
            session.commit()
        except Exception as e:
            session.rollback()
        # session.expunge(item)
        result = to_dict(item)
        session.close()
        return result

class ChildController(MainController):

    @run_on_executor
    @return_future
    def create(self, item, callback=None):
        callback(super().create(item, callback))


def to_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}


metadata = Base.metadata


def create_all():
    metadata.create_all(engine)


class MainHandler(tornado.web.RequestHandler):

    def initialize(self, session):
        self.controller = ChildController(session, User)

    @web.asynchronous
    @gen.coroutine
    def get(self):
        res = yield gen.Task(self.controller.get_all)
        try:
            if isinstance(res.result(), Exception):
                raise res.result()
        # scpefic exc handling
        except Exception as exc:
            raise tornado.web.HTTPError(400, reason='Error!!1 %s' % exc)
        response = json.dumps({'users': [to_dict(user) for user in
                                         res.result()]})
        self.write(response)
        self.set_status(200)
        self.finish()

    @web.asynchronous
    @gen.coroutine
    def post(self):
        data = json.loads(self.request.body.decode('utf-8'))
        name = data.get('name')
        # can be moved to Controller
        item = User(name=name)
        res = yield gen.Task(self.controller.create, item)
        try:
            if isinstance(res.result(), Exception):
                raise res.result()
        # scpefic exc handling
        except Exception as exc:
            raise tornado.web.HTTPError(400, reason='Error!!1, %s' % exc)
        self.write(res.result())
        self.set_status(201)
        self.finish()


def make_app():
    create_all()
    return tornado.web.Application([
        (r"/", MainHandler, dict(
            session=scoped_session(sessionmaker(bind=engine,
                                                expire_on_commit=False)))),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
