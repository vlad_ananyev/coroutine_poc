#!/bin/bash

if [[ ! -e ./.venv/ ]]; then
    virtualenv -p python3.5 .venv
fi
