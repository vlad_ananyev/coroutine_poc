import tornado.ioloop
import json
import tornado.web
from tornado import gen
from sqlalchemy import create_engine, inspect
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from concurrent.futures import ThreadPoolExecutor


engine = create_engine('sqlite:///:memory:', echo=True)
Base = declarative_base()


class User(Base):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    name = Column(String(30), nullable=False)

    def __init__(self, id=None, name=None):
        self.id = id
        self.name = name


def to_dict(obj):
    return {c.key: getattr(obj, c.key)
            for c in inspect(obj).mapper.column_attrs}


metadata = Base.metadata


def create_all():
    metadata.create_all(engine)


class MainController(object):

    def __init__(self):
        self._session = scoped_session(sessionmaker(bind=engine))

    @property
    def session(self):
        return self._session

    def get_users(self):
        return self.session.query(User).all()

    def create_user(self, name):
        user = User(name=name)
        self.session.add(user)
        self.session.commit()


class MainHandler(tornado.web.RequestHandler):

    @gen.coroutine
    def get(self):
        users = MainController().get_users()
        response = json.dumps({'users': [to_dict(user) for user in users]})
        self.write(response)

    @gen.coroutine
    def post(self):
        data = json.loads(self.request.body.decode('utf-8'))
        name = data.get('name')
        controller = MainController()
        controller.create_user(name)
        self.write(data)


def make_app():
    create_all()
    return tornado.web.Application([
        (r"/", MainHandler),
    ])

if __name__ == "__main__":
    app = make_app()
    app.listen(8888)
    tornado.ioloop.IOLoop.current().start()
